export const Images = {
  CALENDAR: require('./Images/calendar.png'),
  CLIPBOARD: require('./Images/clipboard.png'),
  DROPDOWN: require('./Images/down_arrow.png'),
  FILTER: require('./Images/filter.png'),
  MENU: require('./Images/menu.png'),
  REFRESH: require('./Images/refresh.png'),
  SEARCH: require('./Images/search.png'),
  MAP: require('./Images/location.png'),
  AVATAR: require('./Images/avatar_profile.jpeg'),
  HOME: require('./Images/home.png'),
};
