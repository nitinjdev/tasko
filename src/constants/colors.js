module.exports = {
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  RED: '#FE5A1C',
  APP_DARK_ORANGE: '#ff8101',
  APP_ORANGE: '#fe991b',
  APP_LIGHT_YELLOW: '#fdb73c',
  LIGHT_GRAY: '#B6B6B6',
  GREY: '#D3D3D3',
};
