module.exports = {
  HEADING_XL: 30,
  HEADING: 28,
  HEADING_MEDIUM: 24,
  HEADING_XS: 26,
  SUBHEADING_BODY: 21,
  SUBHEADING: 20,
  SUBHEADING_XS: 18,
  SUBHEADING_XL: 22,
  HEADING_LINE_HEIGHT: 64,
  BODY: 18,
  BIG_ICON: 44,
  BODY_LINE_HEIGHT: 18,
  ICONS: 20,
  MEDIUM: 14,
  AVERAGE: 16,
  LARGE: 26,
  SMALL: 12,
  VERY_SMALL: 10,
  BUTTON: 20,
  SMALL_BODY: 16,
  VERY_LARGE: 70,
  HUNDRED: 100,
  FIFTY: 48,
  HEADING_LARGE: 32,
  ICON: 15,
};
