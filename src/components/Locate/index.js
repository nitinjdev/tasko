import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator} from 'react-native';
import HeaderWithIcon from '../Common/HeaderWithIcon';
import {ASSIGN_ME} from '../../constants/Text';
import {VIEWPORT_HEIGHT} from '../../constants/misc';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

const Locate = ({props, navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const getMoviesFromApiAsync = async () => {
    try {
      const response = await fetch(
        'https://run.mocky.io/v3/82f1d43e-2176-4a34-820e-2e0aa4566b5c',
      );
      const json = await response.json();
      setData(json);
      return json;
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getMoviesFromApiAsync();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: 'grey'}}>
      <HeaderWithIcon
        label={ASSIGN_ME}
        disabled={false}
        onPressOfLocation={() => navigation.navigate('Home')}
      />
      {isLoading ? (
        <ActivityIndicator size="large" color="#00ff00" style={{flex:1}}/>
      ) : (
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{
            height: VIEWPORT_HEIGHT,
          }}>
          {data.map(element => {
            const title = `${element.title} ${element.subtitle}`;
            const id = `${element.id}`;
            return (
              <Marker
                key={element.id}
                identifier={id}
                pinColor={'white'} // any color
                title={title}
                description={element.short_desc}
                coordinate={{
                  latitude: element.latitude,
                  longitude: element.longitude,
                }}
              />
            );
          })}
        </MapView>
      )}
    </View>
  );
};

export default Locate;
