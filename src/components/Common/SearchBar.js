import React, {useState} from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StyleSheet,
  Keyboard,
} from 'react-native';
import {Images} from '../../../assets/images';
import {isEmpty} from '../../utils/utils_method';

const SearchBar = props => {
  const {onPressOfSearch, setSeaching} = props;
  const [searchText, setSearchText] = useState('');

  const ontextChaned = text => {
    setSearchText(text);
    if (isEmpty(text)) {
      setSeaching(false);
    }
  };

  const onPressBtn = searchText => {
    onPressOfSearch(searchText);
    Keyboard.dismiss();
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{marginLeft: 10}}
        onPress={() => onPressBtn(searchText)}>
        <Image source={Images.SEARCH} style={styles.imageStyle} />
      </TouchableOpacity>
      <TextInput
        style={styles.searchBar}
        onChangeText={ontextChaned}
        value={searchText}
        // onChangeText={onChangeNumber}
        // value={number}
        placeholder="Search Task"
      />
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    width: '65%',
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 10,
  },
  imageStyle: {width: 20, height: 20, tintColor: 'grey'},
  searchBar: {
    width: '80%',
    fontSize: 15,
    //   backgroundColor: '#d9dbda',
    alignItems: 'center',
  },
});
