import {VIEWPORT_WIDTH, VIEWPORT_HEIGHT} from '../../constants/misc';
import fontsSizes from '../../constants/fontsSizes';
import {DEFAULT_MARGIN} from '../../constants/misc';
import colors from '../../constants/colors';

export default {
  linearGradient: {
    flexDirection: 'row',
    height: VIEWPORT_HEIGHT / 12,
  },
  textView: {
    width: '70%',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: fontsSizes.SUBHEADING,
    fontFamily: 'Gill Sans',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: DEFAULT_MARGIN,
    color: colors.WHITE,
    backgroundColor: 'transparent',
  },
  subContainer: {
    // marginLeft: 5,
    // width: '30%',
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  buttonContainer: {
    flexDirection: 'row',
    width: 90,
    justifyContent: 'space-between',
    height: 50,
    alignItems: 'center',
  },
  imageStyle: {
    width: VIEWPORT_WIDTH / 15,
    height: VIEWPORT_WIDTH / 15,
    tintColor: 'white',
  },
};
