import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {Images} from '../../../assets/images';
import colors from '../../constants/colors';
import DashedLine from 'react-native-dashed-line';
import moment from 'moment';

const extraData = `We use cookies to make interactions with our websites and
services easy and meaningful. For more information about the cookies
we use or to find out how you can disable cookies`;

const CardElement = props => {
  const {title, subtitle, created, short_desc, long_desc} = props;
  const [isCardExpanded, setCardExpanded] = useState(true);
  const [numberLine, setNumberLine] = useState(2);

  const ExpandCard = () => {
    setCardExpanded(!isCardExpanded);
    if (isCardExpanded) {
      setNumberLine(10);
    } else {
      setNumberLine(2);
    }
  };

  return (
    <View
      style={{
        backgroundColor: colors.WHITE,
        width: '93%',
        margin: 15,
        borderRadius: 10,
        height: isCardExpanded ? null : 300,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingVertical: 10,
        }}>
        <View style={{flexDirection: 'row', width: '60%'}}>
          <Image source={Images.AVATAR} style={{width: 50, height: 50}} />
          <View style={{marginHorizontal: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 20}}>{title}</Text>
            <Text style={{fontWeight: 'bold', fontSize: 12}}>{subtitle}</Text>
          </View>
        </View>
        <View
          style={{padding: 5, backgroundColor: '#FCD116', borderRadius: 20}}>
          <Text style={{fontWeight: 'bold', fontSize: 12, color: 'white'}}>
            Assigned
          </Text>
        </View>
      </View>
      <DashedLine
        dashLength={2}
        dashThickness={1}
        dashGap={3}
        dashColor={colors.LIGHT_GRAY}
      />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingVertical: 10,
          width: '55%',
        }}>
        <Image
          source={Images.CALENDAR}
          style={{width: 20, height: 20, marginHorizontal: 50}}
        />
        <View style={{flexDirection: 'row'}}>
          <Text>Created :</Text>
          <Text style={{marginLeft: 10, fontWeight: 'bold'}}>
            {moment(created).format('Do MMM YYYY')}
          </Text>
        </View>
      </View>
      <DashedLine
        dashLength={2}
        dashThickness={1}
        dashGap={3}
        dashColor={colors.LIGHT_GRAY}
      />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          //   justifyContent: 'space-around',
        }}>
        <Image
          source={Images.MENU}
          style={{width: 20, height: 20, marginLeft: 35}}
        />
        <Text style={{width: '70%', marginLeft: 25, fontWeight: 'bold'}}>
          {short_desc}
        </Text>
      </View>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          //   justifyContent: 'space-around',
        }}>
        <Image
          source={Images.CLIPBOARD}
          style={{width: 20, height: 20, marginLeft: 35}}
        />
        <Text
          numberOfLines={numberLine}
          ellipsizeMode="tail"
          style={{width: '70%', marginLeft: 25, fontWeight: 'bold'}}>
          {long_desc}
        </Text>
      </View>

      <TouchableOpacity
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          width: '20%',
          marginLeft: 150,
          //   bottom: 0,
        }}
        onPress={ExpandCard}>
        <Image source={Images.DROPDOWN} style={{width: 20, height: 20}} />
      </TouchableOpacity>
    </View>
  );
};

export default CardElement;
