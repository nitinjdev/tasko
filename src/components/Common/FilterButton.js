import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Images} from '../../../assets/images';
import colors from '../../constants/colors';

const FilterButton = props => {
  const {onPressOfFilter} = props;
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => onPressOfFilter()}>
      <Text style={styles.buttonText}>Filter</Text>
      <Image source={Images.FILTER} style={styles.imageStyle} />
    </TouchableOpacity>
  );
};

export default FilterButton;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.RED,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 10,
    width: '25%',
  },
  imageStyle: {width: 20, height: 20, tintColor: colors.WHITE},
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.WHITE,
  },
});
