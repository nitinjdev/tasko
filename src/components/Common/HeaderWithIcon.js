import React from 'react';
import {Text, TouchableOpacity, Image, View} from 'react-native';
import styles from './styles';
import {
  APP_DARK_ORANGE,
  APP_ORANGE,
  APP_LIGHT_YELLOW,
} from '../../constants/colors';
import LinearGradient from 'react-native-linear-gradient';
import {Images} from '../../../assets/images';

const HeaderWithIcon = props => {
  const {label, onPressOfRefresh, onPressOfLocation, disabled = true} = props;
  return (
    <LinearGradient
      start={{x: 0, y: 1}}
      end={{x: 1, y: 0}}
      locations={[0, 0.4, 0.8]}
      colors={[APP_DARK_ORANGE, APP_ORANGE, APP_LIGHT_YELLOW]}
      style={styles.linearGradient}>
      <View style={styles.textView}>
        <Text style={styles.textStyle}>{label}</Text>
      </View>
      <View style={styles.subContainer}>
        {disabled ? (
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={onPressOfRefresh}>
              <Image source={Images.REFRESH} style={styles.imageStyle} />
            </TouchableOpacity>
            <TouchableOpacity onPress={onPressOfLocation}>
              <Image source={Images.MAP} style={styles.imageStyle} />
            </TouchableOpacity>
          </View>
        ) : (
          <TouchableOpacity onPress={onPressOfLocation}>
            <Image source={Images.HOME} style={styles.imageStyle} />
          </TouchableOpacity>
        )}
      </View>
    </LinearGradient>
  );
};

export default HeaderWithIcon;
