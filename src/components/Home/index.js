import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  Alert,
  Modal,
  Pressable,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useNetInfo} from '@react-native-community/netinfo';
import HeaderWithIcon from '../Common/HeaderWithIcon';
import {ASSIGN_ME} from '../../constants/Text';
import SearchBar from '../Common/SearchBar';
import FilterButton from '../Common/FilterButton';
import CardElement from '../Common/CardElement';
import {VIEWPORT_WIDTH} from '../../constants/misc';
import {isEmpty} from '../../utils/utils_method';
import styles from './styles';

const Home = ({props, navigation}) => {
  const netInfo = useNetInfo();
  const {isConnected} = netInfo;
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isNetworkAvailable, setNetworkAvailable] = useState(isConnected);
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [isSeaching, setSeaching] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [statusArray, setStatusArray] = useState([]);

  const getMoviesFromApiAsync = async () => {
    try {
      const response = await fetch(
        'https://run.mocky.io/v3/82f1d43e-2176-4a34-820e-2e0aa4566b5c',
      );
      const json = await response.json();
      setNetworkAvailable(true);
      setData(json);
      return json;
    } catch (error) {
      setNetworkAvailable(false);
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const getSearchResult = text => {
    setSeaching(true);
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource and update FilteredDataSource
      const newData = data.filter(function (item) {
        // Applying filter for the inserted text in search bar
        const itemData = item.title
          ? item.title.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSource(data);
    }
  };

  const getUniqueStatus = () => {
    setModalVisible(true);
    const val1 = data
      .map(item => item.status)
      .filter((value, index, self) => self.indexOf(value) === index);
    setStatusArray(val1);
    console.log('val1', val1);
  };

  const getResult = () => {
    if (isEmpty(filteredDataSource)) {
      console.log('not fond');
      return (
        <View style={{alignItems: 'center'}}>
          <Text style={{color: 'red', fontSize: 20}}>Result not found</Text>
        </View>
      );
    }
    return (
      <View>
        <Text style={{color: 'black', fontSize: 20, marginLeft: 20}}>
          Search Result :
        </Text>
        <FlatList
          data={filteredDataSource}
          keyExtractor={(item, index) => index.toString()}
          // ItemSeparatorComponent={ItemSeparatorView}
          renderItem={({item}) => (
            <CardElement
              title={item.title}
              subtitle={item.subtitle}
              created={item.created}
              short_desc={item.short_desc}
              long_desc={item.long_desc}
              status={item.status}
            />
          )}
        />
      </View>
    );
  };

  useEffect(() => {
    getMoviesFromApiAsync();
  }, [isNetworkAvailable]);

  return (
    <View style={styles.mainContainer}>
      <HeaderWithIcon
        label={ASSIGN_ME}
        onPressOfLocation={() => navigation.navigate('Locate')}
      />
      <View style={styles.serchContainer}>
        <SearchBar
          onPressOfSearch={getSearchResult}
          setSeaching={setSeaching}
        />
        <FilterButton onPressOfFilter={getUniqueStatus} />
      </View>
      {!isNetworkAvailable && !isLoading && (
        <Text style={{color: 'red', fontSize: 20, marginLeft: 20}}>
          oops Internet connection is lost{' '}
        </Text>
      )}
      <Modal
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          width: '60%',
        }}
        animationType="none"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View
          style={{
            borderRadius: 10,
            borderWidth: 1,
            marginHorizontal: 30,
            width: '80%',
            marginTop: 100,
          }}>
          <View style={styles.modalView}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: 'grey',
              }}>
              <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                Choose option from below
              </Text>
              <Pressable onPress={() => setModalVisible(!modalVisible)}>
                <Text style={{fontSize: 20, fontWeight: 'bold', color: 'red'}}>
                  Close
                </Text>
              </Pressable>
            </View>
            <ScrollView>
              {statusArray.map(element => {
                return (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('FilterScreen', {
                        status: element,
                        data: data,
                      })
                    }
                    style={{
                      backgroundColor: 'white',
                      padding: 5,
                      borderBottomWidth: 1,
                      borderTopWidth: 1,
                    }}>
                    <Text style={{color: 'black'}}>{element}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      </Modal>
      {isLoading ? (
        <ActivityIndicator size="large" color="#00ff00" style={{flex: 1}} />
      ) : isSeaching ? (
        getResult()
      ) : (
        <FlatList
          data={data}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => (
            <CardElement
              title={item.title}
              subtitle={item.subtitle}
              created={item.created}
              short_desc={item.short_desc}
              long_desc={item.long_desc}
              status={item.status}
            />
          )}
        />
      )}
    </View>
  );
};

export default Home;
