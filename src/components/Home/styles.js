import colors from '../../constants/colors';
export default {
  mainContainer: {flex: 1, backgroundColor: colors.GREY},
  serchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
};
