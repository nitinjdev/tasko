import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import CardElement from '../Common/CardElement';
import {ASSIGN_ME} from '../../constants/Text';
import HeaderWithIcon from '../Common/HeaderWithIcon';

const FilterScreen = props => {
  const {status, data} = props.route.params;
  const {navigation} = props;

  const [fData, setfData] = useState([]);

  const filterData = () => {
    const newData = data.filter(function (item) {
      // Applying filter for the inserted text in search bar

      return item.status === status;
    });
    // newArray.push(newData);
    console.log('new data', newData);
    return newData;
  };

  return (
    <View style={{flex: 1}}>
      <HeaderWithIcon
        label={ASSIGN_ME}
        disabled={false}
        onPressOfLocation={() => navigation.navigate('Home')}
      />
      <Text
        style={{
          color: 'black',
          fontSize: 17,
          marginLeft: 20,marginTop:20,
        }}>{`Filtered with status name : ${status}`}</Text>
      <FlatList
        data={filterData()}
        maxToRenderPerBatch={10}
        initialNumToRender={10}
        keyExtractor={({id}, index) => id}
        renderItem={({item}) => (
          <CardElement
            title={item.title}
            subtitle={item.subtitle}
            created={item.created}
            short_desc={item.short_desc}
            long_desc={item.long_desc}
            status={item.status}
          />
        )}
      />
    </View>
  );
};

export default FilterScreen;
